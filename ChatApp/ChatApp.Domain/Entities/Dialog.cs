﻿namespace ChatApp.Domain.Entities;

public class Dialog: BaseEntity
{
    public ICollection<User> Participants { get; set; }
    public ICollection<Message> Messages { get; set; }
    
    public Message? GetLastMessage()
    {
        return this.Messages.Count > 0 ? this.Messages.OrderBy(message => message.DateTimeUtc).Last() : null;
    }
}