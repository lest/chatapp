﻿namespace ChatApp.Domain.Entities;

public class User: BaseEntity
{
    public string UserName { get; set; }
    
    public string PasswordHash { get; set; }
    
    public ICollection<Dialog> Dialogs { get; set; }
}