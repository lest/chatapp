﻿using System.ComponentModel.DataAnnotations;

namespace ChatApp.Domain.Entities;

public class Message: BaseEntity
{
    public int OwnerId { get; set; }
    
    public int DialogId { get; set; }
    
    public string Text { get; set; }
    
    
    public DateTime DateTimeUtc { get; set; }
}