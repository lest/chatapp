﻿using ChatApp.Domain.Entities;

namespace ChatApp.Domain.DataAccess;

public interface IDialogRepository: IDataRepository<Dialog>
{
    Task<Dialog?> GetWithReferences(int dialogId);
}