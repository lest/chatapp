﻿using ChatApp.Domain.Entities;

namespace ChatApp.Domain.DataAccess;

public interface IMessageRepository: IDataRepository<Message>
{
}