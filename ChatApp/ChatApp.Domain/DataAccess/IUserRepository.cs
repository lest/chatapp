﻿using ChatApp.Domain.Entities;

namespace ChatApp.Domain.DataAccess;

public interface IUserRepository: IDataRepository<User>
{
    Task<User?> GetWithDialogs(int userId);
    Task<User?> FindByUserName(string userName);
    Task<IList<User>> SearchUsers(string searchValue);
}