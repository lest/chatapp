﻿using System.Linq.Expressions;

namespace ChatApp.Domain.DataAccess;

public interface IDataRepository<T> where T : BaseEntity
{ 
    Task Remove(long id);
    void Remove(T entity);

    Task<List<T>> ListAll();

    Task<T> AddAsync(T item);

    void Attach(T item);

    void AddRange(IEnumerable<T> item);

    Task SaveChangesAsync();

    Task<T?> Get(long id);

    Task<List<T>> Get(IEnumerable<int> ids);

    Task<T?> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate);

    Task<List<T>> Where(Expression<Func<T, bool>> predicate);

    Task<bool> Any(Expression<Func<T, bool>> predicate);

    Task<int> Count();

    Task RemoveSoft(long id);
}