﻿using System.ComponentModel.DataAnnotations;

namespace ChatApp.Domain;

public class BaseEntity
{
    protected BaseEntity()
    {   
        CreatedAt = DateTime.UtcNow;
        IsDeleted = false;
    }
    
    [Key]
    public int Id { get; set; }
    public DateTime? CreatedAt { get; }
    public bool IsDeleted { get; set; }
}