﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OA.Persistence;

namespace ChatApp.DataAccessExtensions;

public static class DatabaseStartupExtensions
{
    public static void AddSqlServer(this IServiceCollection services, string connectionString)
    {
        services.AddDbContext<ApplicationDbContext>(
            options => options.UseNpgsql(connectionString)
        );
    }
}