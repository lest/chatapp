﻿using ChatApp.Domain.DataAccess;
using ChatApp.Persistence.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace ChatApp.DataAccessExtensions;

public static class DataAccessStartupExtensions
{
    public static void AddCommonRepositories(this IServiceCollection services)
    {
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IDialogRepository, DialogRepository>();
        services.AddScoped<IMessageRepository, MessageRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
    }
}