﻿namespace ChatApp.Common;

public class ApiMessage
{
    public static ApiMessage Error(string text) => new(ApiMessageType.Error, text);

    public static ApiMessage Info(string text) => new(ApiMessageType.Info, text);

    public ApiMessage(string type, string text)
    {
        Level = type;
        Text = text;
    }

    public string Level { get; }

    public string Text { get; }
}