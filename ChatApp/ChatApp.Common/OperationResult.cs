﻿namespace ChatApp.Common;

public class OperationResult : OperationResult<Nothing>
{
    public static OperationResult Success(params ApiMessage[] messages)
    {
        return new OperationResult(true, messages);
    }

    public static OperationResult Failure(params ApiMessage[] messages)
    {
        return new OperationResult(false, messages);
    }

    public OperationResult(bool success, params ApiMessage[] messages) : base(success, Nothing.Value, messages)
    {
    }
}

public class OperationResult<T> : IOperationResult<T> where T : class
{
    private readonly ApiMessage[] _messages;

    public static OperationResult<T> SuccessOf(T data, params ApiMessage[] messages)
    {
        return new OperationResult<T>(true, data, messages);
    }

    public static OperationResult<T> FailureOf(params ApiMessage[] messages)
    {
        return new OperationResult<T>(false, null, messages);
    }

    public OperationResult(bool success, T data, params ApiMessage[] messages)
    {
        _messages = messages;
        IsSuccess = success;
        Data = data;
    }

    public T Data { get; }

    public IList<ApiMessage> Messages => _messages;

    public bool IsSuccess { get; }
}