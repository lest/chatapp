﻿namespace ChatApp.Common;

public interface IOperationResult
{
    IList<ApiMessage> Messages { get; }

    bool IsSuccess { get; }
}

public interface IOperationResult<out T> : IOperationResult where T : class
{
    T Data { get; }
}