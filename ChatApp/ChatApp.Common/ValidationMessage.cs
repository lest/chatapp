﻿namespace ChatApp.Common;

public class ValidationMessage : ApiMessage
{
    public ValidationMessage(string field, string text) : base(ApiMessageType.Validation, text)
    {
        Field = field;
    }

    public string Field { get; }
}