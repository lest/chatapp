﻿namespace ChatApp.Common;

public static class ApiMessageType
{
    public static string Info = "Info";
    public static string Error = "Error";
    public static string Validation = "Validation";
}