﻿namespace ChatApp.Common;

public class Nothing
{
    public static readonly Nothing Value = new();

    private Nothing()
    {
    }
}