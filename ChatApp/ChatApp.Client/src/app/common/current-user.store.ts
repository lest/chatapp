import { Injectable } from '@angular/core';
import {User} from "./domain";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({ providedIn: 'root'})
export class CurrentUserStore {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor() {
    const
      storedUser: any = JSON.parse(localStorage.getItem('currentUser')),
      user = storedUser == null ? null : storedUser;

    this.currentUserSubject = new BehaviorSubject<User>(user);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  setCurrentUser(currentUser: User|null) {
    this.currentUserSubject.next(currentUser);
  }

  getCurrentUser(): User | null {
    return this.currentUserSubject.value;
  }

  public listenChangeCurrentUser(): Observable<User> {
    return this.currentUserSubject;
  }
}

