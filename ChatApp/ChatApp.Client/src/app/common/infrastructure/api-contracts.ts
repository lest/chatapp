export interface IOperationMessage {
  level: string;
  text: string;
}

export interface IOperationResult {
  isSuccess: boolean;
  data: any;
  messages: IOperationMessage[];
}
