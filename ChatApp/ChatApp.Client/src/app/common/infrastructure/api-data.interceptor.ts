import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpEvent,
  HttpHandler,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NotificationStore } from "../notifications/notifications.store";
import { Notification, NotificationLevel } from "../notifications/notification";


@Injectable()
export class NotificationHttpInterceptor implements HttpInterceptor {

  constructor(
    private notificationStore: NotificationStore,
    private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const successMap = (event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        const response = event as HttpResponse<any>;

        if (response.body && response.body.data && response.body.data.unAuthorizedRequest) {
          this.router.navigate(['/']);
        } else if (response.body && response.body.messages) {
          this.handleMessages(response.body.messages);
        }
      }

      return event;
    };

    const errorMap = (err: any, caught: any) => {
      if (err instanceof HttpErrorResponse) {
        var httpErrorResponse = <HttpErrorResponse>err;
        if (httpErrorResponse.error && !httpErrorResponse.error.isSuccess && httpErrorResponse.error.messages != null) {
          const messages = httpErrorResponse.error.messages;
          this.handleMessages(messages);
        } else {
          this.notificationStore.push(new Notification('HTTP error while fetching response: ' + err.status, NotificationLevel.Error));
          if (err.status === 401) {
            //todo signOut
            this.router.navigate(['/']);
          }
        }
      }

      return throwError(err);
    }

    return next
      .handle(req)
      .pipe(
        map(successMap),
        catchError(errorMap));
  }

  private handleMessages(messages: { level: string, text: string }[]) {
    for (let i = 0; i < messages.length; i++) {
      const message = messages[i];

      switch (message.level) {
        case 'Info':
          this.notificationStore.push(new Notification(message.text, NotificationLevel.Info));
          break;
        case 'Error':
          this.notificationStore.push(new Notification(message.text, NotificationLevel.Error));
          break;
      }
    }
  }
}
