import {IOperationResult} from "./api-contracts";

export function MAP_OPERATION_RESULT<T>(map: (dto: any) => T): (result: IOperationResult) => T {
  return (result: IOperationResult) => {
    if (result.isSuccess) {
      if (result.data){
        return map(result.data);
      }
      else return result.data;

    }

    throw new Error('Unexpected error while calling API');
  };
}

export function UNWRAP_OPERATION_RESULT<T>(): (result: IOperationResult) => T {
  return (result: IOperationResult) => {
    if (result.isSuccess) {
      return result.data;
    }

    throw new Error('Unexpected error while calling API');
  };
}

export function MAP_OPERATION_RESULT_ARRAY<T>(map: (dto: any) => T): (result: IOperationResult) => T[] {
  return MAP_OPERATION_RESULT(dtos => {
    return dtos.map(map);
  });
}

export function MAP_NO_RESULT(result: IOperationResult): void {
  return MAP_OPERATION_RESULT(() => { })(result);
}
