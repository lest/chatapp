export enum NotificationLevel {
  Info = 0,
  Error = 1
}

export class Notification {
  constructor(
    public readonly text: string,
    public readonly level: NotificationLevel) { }
}
