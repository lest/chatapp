export interface Dialog {
  id: number;
  participants: User[];
}

export interface DialogListItem extends Dialog{
  lastMessage: Message;
}

export interface DialogDetailsItem extends Dialog{
  messages: Message[];
}

export interface Message {
  id?: number;
  ownerId: number;
  dialogId: number;
  text: string;
  dateTime: Date;
}

export interface User{
  id: number;
  userName: string;
  token?: string;
}
