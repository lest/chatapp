import { Component, Input } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-loading-container',
  templateUrl: './loading-container.component.html',
  styleUrls: ['./loading-container.component.scss'],
  animations: [
    trigger('openClose', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('100ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate('300ms', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LoadingContainerComponent {
  loadingInternal = false;

  @Input()
  get loading() {
    return this.loadingInternal;
  }
  set loading(value: boolean) {
    if (!value) {
      setTimeout(() => {
        this.loadingInternal = false;
      }, 100);
    } else {
      this.loadingInternal = value;
    }
  }
}
