import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {DialogDetailsItem, User} from "../domain";
import {MAP_OPERATION_RESULT, MAP_OPERATION_RESULT_ARRAY} from "../infrastructure/map-operation-result";
import {map} from "rxjs/operators";
import {IOperationResult} from "../infrastructure/api-contracts";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  public authenticate(authenticateRequest : { username, password }) {
    return this.http.post<IOperationResult>('api/account/authenticate', authenticateRequest).pipe(map(MAP_OPERATION_RESULT<User>(dto => dto)));
  }

  public searchUsers(searchValue: string) {
    return this.http
      .get<IOperationResult>('/api/account/search?searchValue=' + searchValue).pipe(map(MAP_OPERATION_RESULT_ARRAY<User>(dto => dto)));
  }

}
