import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import { Message } from "../domain";
import { HubConnectionBuilder, HubConnection, HubConnectionState } from '@microsoft/signalr';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SignalrService {

  onMessageReceiveEvent = new BehaviorSubject<Message>(null);
  onConnected = new BehaviorSubject<boolean>(null);

  dialogId: number;

  private hubConnection: HubConnection;

  constructor() {
  }

  start(){
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(environment.hubUrl)
      .withAutomaticReconnect()
      .build();

    this.hubConnection.start().then(() => {
      console.log('Connection started');
      this.onConnected.next(true);
    }).catch(err => console.log('Error while starting connection: ' + err));
  }

  stop(): void {
    if (this.hubConnection.state === HubConnectionState.Connected) {
      this.hubConnection.stop();
      this.onConnected = new BehaviorSubject<boolean>(null);
    }
  }

  connectToDialogRealtime(id: number) {
    this.onConnected.subscribe((value) => {
      if (value){
        this.dialogId = id;
        this.hubConnection.invoke('ListenDialog', id).then(() => {
          console.log('Connected to Dialog ' + id);
        });

        this.hubConnection.on('ReceiveMessage', (message) => {
          console.log('Receive Message', message);
          this.handleMessage(message);
        });
      }
    });
  }

  disconnectDialog(id: number|null){
    if (id){
      this.hubConnection.off('ReceiveMessage');
      this.hubConnection.invoke('UnsubscribeDialog', id).then(() => {
        console.log('Disconnected from Dialog ' + id);
      });
    }
  }

  sendToServer(message: Message){
    this.hubConnection.invoke('SendMessageToCaller', message).then(() => {
      console.log('Message sent to server ' + message);
    });
  }

  private handleMessage(message: Message) {
    this.onMessageReceiveEvent.next(message);
  }
}
