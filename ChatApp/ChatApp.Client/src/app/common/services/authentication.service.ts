import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {UserService} from "./user.service";
import {CurrentUserStore} from "../current-user.store";

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(
      private userService: UserService,
      private currentUserStore: CurrentUserStore) {
    }

    login(username: string, password: string) {
      return this.userService.authenticate({ username, password })
            .pipe(map(result => {
              localStorage.setItem('currentUser', JSON.stringify(result));
              this.currentUserStore.setCurrentUser(result);
              return result;
            }));
    }

    logout() {
      localStorage.removeItem('currentUser');
      this.currentUserStore.setCurrentUser(null);
    }
}
