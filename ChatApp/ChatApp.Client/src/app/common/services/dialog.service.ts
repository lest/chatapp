import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {DialogDetailsItem, DialogListItem} from "../domain";
import {MAP_OPERATION_RESULT, MAP_OPERATION_RESULT_ARRAY} from "../infrastructure/map-operation-result";
import {map} from "rxjs/operators";
import {IOperationResult} from "../infrastructure/api-contracts";

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private http: HttpClient) {
  }

  public listDialogs(userId: number) {
    return this.http
      .get<IOperationResult>('/api/dialog/list?userId=' + userId).pipe(map(MAP_OPERATION_RESULT_ARRAY<DialogListItem>(dto => dto)));
  }

  public getDialog(id: number) {
    return this.http
      .get<IOperationResult>('/api/dialog/get?dialogId=' + id).pipe(map(MAP_OPERATION_RESULT<DialogDetailsItem>(dto => dto)));
  }

  public createDialog(participantIds: number[]) {
    return this.http
      .post<DialogListItem>('/api/dialog/create', participantIds);
  }
}
