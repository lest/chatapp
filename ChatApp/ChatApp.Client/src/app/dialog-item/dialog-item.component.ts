import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {DialogListItem, User} from "../common/domain";
import {CurrentUserStore} from "../common/current-user.store";

@Component({
  selector: 'app-dialog-item',
  templateUrl: './dialog-item.component.html',
  styleUrls: ['./dialog-item.component.scss']
})
export class DialogItemComponent implements OnInit {

  @Input()
  dialog: DialogListItem = null;

  @Input() isSelected = false;

  @Output()
  dialogSelected = new EventEmitter();

  currentUser: User;

  constructor(private currentUserStore: CurrentUserStore) { }

  ngOnInit(): void {
    this.currentUser = this.currentUserStore.getCurrentUser();
  }

  findParticipant(participants: User[]) {
    return participants.find(participant => participant.id !== this.currentUser.id);
  }

  openExisting(dialog: DialogListItem) {
    this.dialogSelected.emit(dialog);
  }

}
