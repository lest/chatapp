import { Component } from '@angular/core';
import {User} from "./common/domain";
import {CurrentUserStore} from "./common/current-user.store";
import {AuthenticationService} from "./common/services/authentication.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'chat-app';

  currentUser: User;

  constructor(private currentUserStore: CurrentUserStore,
              private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.currentUserStore
      .listenChangeCurrentUser()
      .subscribe(user => {
        this.currentUser = user;
      });
  }

  logout() {
    this.authenticationService.logout();
    this.currentUser = null;
    this.router.navigate(['/login']);
  }
}
