import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NotFound404Component} from "./not-found404.component";
import {DialogListComponent} from "./dialog-list/dialog-list.component";
import {LoginComponent} from "./auth/login/login.component";
import {AuthGuard} from "./auth/helpers/auth.guard";
import {UserPageComponent} from "./user-page/user-page.component";

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'user',
        component: UserPageComponent
      },
      {
        path: 'dialog',
        component: DialogListComponent
      },
      {
        path: '',
        redirectTo: 'dialog',
        pathMatch: 'full',
      }
    ],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '**', component: NotFound404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
