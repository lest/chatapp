import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {DialogDetailsItem, DialogListItem, Message, User} from "../common/domain";
import {DialogService} from "../common/services/dialog.service";
import {SignalrService} from "../common/services/signalr.service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-message-process',
  templateUrl: './message-process.component.html',
  styleUrls: ['./message-process.component.scss']
})
export class MessageProcessComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  selectedDialog: DialogListItem = null;

  //todo eliminate this
  @Input()
  currentUser: User = null;

  detailedDialog: DialogDetailsItem = null;

  messageControl = this.formBuilder.control(null);

  form: FormGroup = this.formBuilder.group({
    message: this.messageControl
  });

  constructor(private dialogService: DialogService,
              private formBuilder: FormBuilder,
              private signalrService: SignalrService) { }

  ngOnInit(): void {
    this.signalrService.start();
  }

  sendMessage(){
    const message = {text: this.messageControl.value, dateTime: new Date(), ownerId: this.currentUser.id, dialogId: this.detailedDialog.id};
    this.detailedDialog.messages.push(message);
    this.signalrService.sendToServer(message);
    this.messageControl.setValue(null);
  }

  receiveMessage(message: Message){
    if (message && message.ownerId != this.currentUser.id){
      this.detailedDialog.messages.push(message);
    }
  }

  ngOnDestroy(): void {
    this.signalrService.disconnectDialog(this.detailedDialog?.id);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.selectedDialog){
      this.dialogService.getDialog(this.selectedDialog.id).subscribe(data =>{
        this.signalrService.disconnectDialog(this.detailedDialog === null ? null : this.detailedDialog.id);
        this.detailedDialog = data;
        this.signalrService.connectToDialogRealtime(this.detailedDialog.id);
        this.signalrService.onMessageReceiveEvent.subscribe(data =>{
          this.receiveMessage(data);
        })
      })
    }
  }



}
