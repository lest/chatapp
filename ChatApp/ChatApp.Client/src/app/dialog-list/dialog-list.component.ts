import {Component, OnDestroy, OnInit} from '@angular/core';
import {DialogService} from "../common/services/dialog.service";
import {DialogListItem, User} from "../common/domain";
import {CurrentUserStore} from "../common/current-user.store";
import {FormBuilder} from "@angular/forms";
import {UserService} from "../common/services/user.service";
import {Subject} from "rxjs";
import {debounceTime, delay, tap, filter, map, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-dialog-list',
  templateUrl: './dialog-list.component.html',
  styleUrls: ['./dialog-list.component.scss']
})
export class DialogListComponent implements OnInit, OnDestroy {

  dialogs: DialogListItem[] = [];
  filteredUsers: User[] = [];

  selectedDialog: DialogListItem = null;
  dialogContentLoading: boolean = false;

  searchSelectControl = this.formBuilder.control(null);
  searchFieldControl = this.formBuilder.control(null);
  searching = false;

  currentUser: User;

  protected _onDestroy = new Subject<void>();

  constructor(private dialogService: DialogService,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private currentUserStore: CurrentUserStore) { }

  ngOnInit(): void {
    this.currentUser = this.currentUserStore.getCurrentUser();
    this.dialogContentLoading = true;
    this.dialogService.listDialogs(this.currentUser.id).subscribe( data => {
      this.dialogs = data;
      this.dialogContentLoading = false;
    });

    this.searchFieldControl.valueChanges
      .pipe(
        filter(search => !!search),
        tap(() => this.searching = true),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(search => {
          return this.userService.searchUsers(search);
        }),
        delay(500),
        takeUntil(this._onDestroy)
      )
      .subscribe(filtered => {
          this.searching = false;
          filtered.subscribe(data => {
            this.filteredUsers = data;
          })
        });
  }

  startDialog() {
    this.dialogService.createDialog([this.currentUser.id, this.searchSelectControl.value.id]).subscribe(data =>{
      this.selectedDialog = data;
      this.dialogs.push(data);
    });
  }

  onDialogSelected(dialog: DialogListItem) {
    this.selectedDialog = dialog;
  }

  searchFieldChange($event: any) {
    if (this.filteredUsers?.length > 0 && this.searchSelectControl.value && !this.filteredUsers?.find(user => user.id === this.searchSelectControl.value.id)){
      this.searchSelectControl.setValue(null);
    }
    if (!this.filteredUsers || this.filteredUsers.length === 0){
      this.searchSelectControl.setValue(null);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
