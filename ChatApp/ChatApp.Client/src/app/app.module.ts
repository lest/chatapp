import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterialModules} from "./material-modules";
import {ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NotFound404Component} from "./not-found404.component";
import { DialogListComponent } from './dialog-list/dialog-list.component';
import {NotificationStore} from "./common/notifications/notifications.store";
import {NotificationHttpInterceptor} from "./common/infrastructure/api-data.interceptor";
import {MatSnackBar} from "@angular/material/snack-bar";
import {NotificationLevel} from "./common/notifications/notification";
import {Router} from "@angular/router";
import { MessageProcessComponent } from './message-process/message-process.component';
import {JwtInterceptor} from "./auth/helpers/jwt.interceptor";
import {LoginComponent} from "./auth/login/login.component";
import { DialogItemComponent } from './dialog-item/dialog-item.component';
import { UserPageComponent } from './user-page/user-page.component';
import {LoadingContainerComponent} from "./common/loading-container/loading-container.component";
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  declarations: [
    AppComponent,
    NotFound404Component,
    DialogListComponent,
    LoadingContainerComponent,
    MessageProcessComponent,
    LoginComponent,
    DialogItemComponent,
    UserPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule,
    HttpClientModule,
    MaterialModules,
    BrowserAnimationsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: NotificationHttpInterceptor, multi: true, deps: [NotificationStore, Router] },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: APP_INITIALIZER,
      useFactory: (store: NotificationStore, injector: Injector) => () => {
        store.getStream().subscribe(notification => {
          const service = injector.get(MatSnackBar);
          service.open(notification.text,
            undefined,
            { panelClass: NotificationLevel[notification.level].toLowerCase(), duration: 3000 });
        });
      },
      multi: true,
      deps: [NotificationStore, Injector]
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
