﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Text;
using ChatApp.Domain.DataAccess;
using ChatApp.Support.Settings;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ChatApp.Support.AuthHelpers;

public class JwtMiddleware
{
    private readonly RequestDelegate _next;
    private readonly AuthSettings _authSettings;

    public JwtMiddleware(RequestDelegate next, IOptions<AuthSettings> authSettings)
    {
        _next = next;
        _authSettings = authSettings.Value;
    }

    public async Task Invoke(HttpContext context, IUserRepository userRepository)
    {
        var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

        if (token != null)
        {
            try
            {
                attachUserToContext(context, userRepository, token);
            }
            catch
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return;
            }
        }
            
        await _next(context);
    }

    private void attachUserToContext(HttpContext context, IUserRepository userRepository, string token)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_authSettings.Secret);
        tokenHandler.ValidateToken(token, new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false,
            ClockSkew = TimeSpan.Zero
        }, out SecurityToken validatedToken);

        var jwtToken = (JwtSecurityToken)validatedToken;
        var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

        var user = userRepository.Get(userId);
        user.Wait();
        context.Items["User"] = user.Result;
    }
}