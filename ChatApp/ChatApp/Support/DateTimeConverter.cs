﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace ChatApp.Support;

public class DateTimeConverter : JsonConverter<DateTime> 
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return new DateTime(
            DateTimeOffset.FromUnixTimeMilliseconds(reader.GetInt64()).DateTime.Ticks,
            DateTimeKind.Utc);
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        long result = new DateTimeOffset(new DateTime(value.Ticks, DateTimeKind.Utc)).ToUnixTimeMilliseconds();

        writer.WriteNumberValue(result);
    }
}