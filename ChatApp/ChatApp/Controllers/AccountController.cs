using ChatApp.Common;
using ChatApp.Services;
using ChatApp.Services.Dto;
using ChatApp.Support.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ChatApp.Controllers;

[ApiController]
[Route("api/account")]
public class AccountController : ControllerBase
{
    private readonly UserService _userService;
    private readonly AuthSettings _authSettings;

    public AccountController(UserService userService, IOptions<AuthSettings> authSettings)
    {
        _userService = userService;
        _authSettings = authSettings.Value;
    }
    
    [AllowAnonymous]
    [HttpPost("authenticate")]
    public async Task<IOperationResult> Authenticate(AuthenticateRequest model)
    {
        return await _userService.Authenticate(model, _authSettings.Secret);
    }
    
    [Support.AuthHelpers.Authorize]
    [HttpGet]
    [Route("search")]
    public async Task<IOperationResult> SearchUsers(string searchValue)
    {
        return await _userService.SearchUsers(searchValue);
    }
}