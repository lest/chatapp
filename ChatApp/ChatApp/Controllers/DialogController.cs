﻿using ChatApp.Common;
using ChatApp.Services;
using ChatApp.Support.AuthHelpers;
using Microsoft.AspNetCore.Mvc;

namespace ChatApp.Controllers;

[ApiController]
[Authorize]
[Route("api/dialog")]
public class DialogController : ControllerBase
{
    private readonly DialogService _dialogService;

    public DialogController(DialogService dialogService)
    {
        _dialogService = dialogService;
    }

    [HttpPost]
    [Route("create")]
    public async Task<IActionResult> Create([FromBody]List<int> participantIds)
    {
        var dialog = await _dialogService.CreateDialog(participantIds);
        return Ok(dialog);
    }

    [HttpGet]
    [Route("list")]
    public async Task<IOperationResult> GetDialogsForUser(int userId)
    {
        return await _dialogService.GetDialogsForUser(userId);
    }

    [HttpGet]
    [Route("get")]
    public async Task<IOperationResult> GetDialog(int dialogId)
    {
        return await _dialogService.GetDialogById(dialogId);
    }
}