using ChatApp.DataAccessExtensions;
using ChatApp.Realtime;
using ChatApp.Services;
using ChatApp.Services.PasswordHasher;
using ChatApp.Services.RealtimeContracts;
using ChatApp.Support.AuthHelpers;
using ChatApp.Support.Settings;

var builder = WebApplication.CreateBuilder(args);

IConfiguration configuration = builder.Configuration;

builder.Services.AddControllers();
//builder.Services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new DateTimeConverter()));  
builder.Services.AddSignalR();
builder.Services.AddSingleton<IRealtimeBroker, SignalRRealtimeBroker>();
builder.Services.Configure<AuthSettings>(configuration.GetSection("AuthSettings"));
builder.Services.AddSingleton<IAccountPasswordHasher>(new NativeAccountPasswordHasher());


builder.Services.AddScoped<UserService>();
builder.Services.AddScoped<DialogService>();
builder.Services.AddScoped<MessageService>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCommonRepositories();
builder.Services.AddSqlServer(configuration.GetConnectionString("Postgre"));
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy", builder => builder
        .WithOrigins("http://localhost:4200")
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials());
});


var app = builder.Build();



if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseMiddleware<JwtMiddleware>();


/*app.UseHttpsRedirection();*/
app.UseCors("CorsPolicy");
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.MapHub<ChatHub>("/realtime");

app.Run();