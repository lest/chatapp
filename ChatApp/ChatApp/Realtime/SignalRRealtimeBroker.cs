﻿using ChatApp.Services.Dto;
using ChatApp.Services.RealtimeContracts;
using Microsoft.AspNetCore.SignalR;

namespace ChatApp.Realtime;

public class SignalRRealtimeBroker: IRealtimeBroker
{
    private readonly IHubContext<ChatHub> _hubContext;

    public SignalRRealtimeBroker(IHubContext<ChatHub> hubContext)
    {
        _hubContext = hubContext;
    }

    public async Task SendMessage(int dialogId, MessageDto message)
    {
        await _hubContext.Clients.Group(dialogId.ToString()).SendAsync("message", message);
    }
}