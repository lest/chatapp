﻿using ChatApp.Services;
using ChatApp.Services.Dto;

namespace ChatApp.Realtime;
using Microsoft.AspNetCore.SignalR;

public class ChatHub: Hub
{
    private readonly DialogService _dialogService;
    
    public ChatHub(DialogService dialogService)
    {
        _dialogService = dialogService;
    }

    public async Task ListenDialog(int dialogId)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, dialogId.ToString());
    }
        
    public async Task UnsubscribeDialog(int dialogId)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, dialogId.ToString());
    }
    
    public async Task SendMessageToCaller(MessageDto message)
    {
        await _dialogService.AddMessageToDialog(message);
        await Clients.Caller.SendAsync("ReceiveMessage", message);
    }
}