﻿using ChatApp.Domain.Entities;

namespace ChatApp.Services.PasswordHasher;

public interface IAccountPasswordHasher
{
    string Hash(User user, string password);

    bool Verify(User user, string providedPassword);
}