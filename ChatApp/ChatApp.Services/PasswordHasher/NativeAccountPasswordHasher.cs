﻿using ChatApp.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace ChatApp.Services.PasswordHasher;

public class NativeAccountPasswordHasher : IAccountPasswordHasher
{
    private readonly IPasswordHasher<User> _nestedHasher = new PasswordHasher<User>();

    public string Hash(User user, string password)
    {
        return _nestedHasher.HashPassword(user, password);
    }

    public bool Verify(User user, string providedPassword)
    {
        PasswordVerificationResult passwordVerificationResult = _nestedHasher.VerifyHashedPassword(
            user, 
            user.PasswordHash, 
            providedPassword);

        return passwordVerificationResult == PasswordVerificationResult.Success;
    }
}