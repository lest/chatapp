﻿using ChatApp.Common;
using ChatApp.Domain.DataAccess;
using ChatApp.Domain.Entities;
using ChatApp.Services.Dto;

namespace ChatApp.Services;

public class DialogService
{
    private readonly IDialogRepository _dialogRepository;
    private readonly IUserRepository _userRepository;

    public DialogService(IDialogRepository dialogRepository, IUserRepository userRepository)
    {
        _dialogRepository = dialogRepository;
        _userRepository = userRepository;
    }

    public async Task<DialogListItemDto> CreateDialog(IList<int> participantIds)
    {
        var participants = await _userRepository.Get(participantIds);
        var dialog = new Dialog
        {
            Participants = participants
        };

        var result = await _dialogRepository.AddAsync(dialog);
        return new DialogListItemDto
        {
            Id = result.Id,
            LastMessage = null,
            Participants = result.Participants.Select(UserToDto).ToList()
        };
    }
    
    public async Task<MessageDto> AddMessageToDialog(MessageDto messageDto)
    {
        var dialog = await _dialogRepository.GetWithReferences(messageDto.DialogId);
        var message = new Message
        {
            Text = messageDto.Text,
            DialogId = messageDto.DialogId,
            OwnerId = messageDto.OwnerId,
            DateTimeUtc = messageDto.DateTime
        };

        dialog?.Messages.Add(message);
        await _dialogRepository.SaveChangesAsync();
        return new MessageDto
        {
            Id = message.Id,
            Text = message.Text,
            DateTime = message.DateTimeUtc,
            DialogId = message.DialogId,
            OwnerId = message.OwnerId
        };
    }

    public async Task<IOperationResult> GetDialogsForUser(int userId)
    {
        var result = await _userRepository.GetWithDialogs(userId);

        if (result != null)
        {
            return OperationResult<IEnumerable<DialogListItemDto>>.SuccessOf(result.Dialogs.Select(dialog => new DialogListItemDto
            {
                Id = dialog.Id,
                LastMessage = MessageToDto(dialog.GetLastMessage()),
                Participants = dialog.Participants.Select(UserToDto).ToList()
            }));
        }

        return OperationResult.FailureOf(ApiMessage.Error($"User with {userId} not found"));
    }

    public async Task<IOperationResult> GetDialogById(int dialogId)
    {
        var result = await _dialogRepository.GetWithReferences(dialogId);
        if (result != null)
        {
            return OperationResult<DialogDetailsDto>.SuccessOf(new DialogDetailsDto
            {
                Id = result.Id,
                Participants = result.Participants.Select(UserToDto).ToList(),
                Messages = result.Messages.Select(MessageToDto).ToList()
            });
        }

        return OperationResult.FailureOf(ApiMessage.Error($"Dialog with {dialogId} not found"));
    }
    
    private static MessageDto MessageToDto(Message? message)
    {
        if (message != null)
        {
            return new MessageDto
            {
                Id = message.Id,
                OwnerId = message.OwnerId,
                Text = message.Text,
                DateTime = message.DateTimeUtc
            };
        }

        return null;

    }
    
    private static UserDto UserToDto(User user)
    {
        return new UserDto
        {
            Id = user.Id,
            UserName = user.UserName
        };
    }
}