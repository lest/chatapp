﻿using ChatApp.Services.Dto;

namespace ChatApp.Services.RealtimeContracts;

public interface IRealtimeBroker
{
    Task SendMessage(int dialogId, MessageDto message);
}