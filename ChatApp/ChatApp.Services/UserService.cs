﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ChatApp.Common;
using ChatApp.Domain.DataAccess;
using ChatApp.Domain.Entities;
using ChatApp.Services.Dto;
using ChatApp.Services.PasswordHasher;
using Microsoft.IdentityModel.Tokens;

namespace ChatApp.Services;

public class UserService
{
    private readonly IUserRepository _userRepository;
    private readonly IAccountPasswordHasher _passwordHasher;

    public UserService(IUserRepository userRepository, IAccountPasswordHasher passwordHasher)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
    }

    public async Task<IOperationResult> Authenticate(AuthenticateRequest model, string secret)
    {
        var user = await _userRepository.FindByUserName(model.UserName);

        if (user == null) 
            return OperationResult<UserDto>.FailureOf(ApiMessage.Error($"Username {model.UserName} does not exist"));

        if (!_passwordHasher.Verify(user, model.Password)) return OperationResult<UserDto>.FailureOf(ApiMessage.Error("Wrong password"));;

        var token = GenerateJwtToken(user, secret);

        return OperationResult<UserDto>.SuccessOf(MapUser(user, token));

    }
    
    public async Task<IOperationResult> SearchUsers(string searchValue)
    {
        var users = await _userRepository.SearchUsers(searchValue);

        if (users.Count == 0) 
            return OperationResult<List<User>>.SuccessOf(null);

        return OperationResult<List<UserDto>>.SuccessOf(users.Select(user => MapUser(user)).ToList());
    }

    private string GenerateJwtToken(User user, string secret)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(secret);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
                
            Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
    
    private UserDto MapUser(User user, string token = null)
    {
        return new UserDto
        {
            Id = user.Id,
            UserName = user.UserName,
            Token = token
        };
    }
}