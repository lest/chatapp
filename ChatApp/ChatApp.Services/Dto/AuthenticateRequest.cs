﻿using System.ComponentModel.DataAnnotations;

namespace ChatApp.Services.Dto;

public record AuthenticateRequest
{
    [Required]
    public string UserName { get; init; }
    
    [Required]
    public string Password { get; init; }
}