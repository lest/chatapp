﻿namespace ChatApp.Services.Dto;

public record UserDto
{
    public int Id { get; init; }
    public string UserName { get; init; }
    public string? Token { get; init; }
}