﻿namespace ChatApp.Services.Dto;

public record DialogDto
{
    public int Id { get; init; }

    public IList<UserDto> Participants { get; init; }
}

public record DialogDetailsDto: DialogDto
{
    public IList<MessageDto> Messages { get; init; }
}

public record DialogListItemDto: DialogDto
{
    public MessageDto? LastMessage { get; init; }
}