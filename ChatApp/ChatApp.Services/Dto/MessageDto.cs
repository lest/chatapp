﻿namespace ChatApp.Services.Dto;

public record MessageDto
{
    public int Id { get; init; }
    
    public int OwnerId { get; init; }
    
    public int DialogId { get; init; }

    public string Text { get; init; }
    
    public DateTime DateTime { get; init; }
}