﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ChatApp.Persistence.Migrations
{
    public partial class ChangedDialogScheme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Dialog");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                table: "Dialog");

            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "Message",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Message");

            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "Dialog",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecipientId",
                table: "Dialog",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
