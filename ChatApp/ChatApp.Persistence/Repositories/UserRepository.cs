﻿using ChatApp.Domain.DataAccess;
using ChatApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using OA.Persistence;

namespace ChatApp.Persistence.Repositories;

public class UserRepository: DataRepository<User>, IUserRepository
{
    public UserRepository(ApplicationDbContext dataContext) : base(dataContext)
    {
    }


    public async Task<User?> GetWithDialogs(int userId)
    {
        return await Entities
            .Include(x => x.Dialogs).ThenInclude(x => x.Participants)
            .Include(x => x.Dialogs).ThenInclude(x => x.Messages)
            .SingleOrDefaultAsync(x => x.Id == userId);
    }

    public async Task<User?> FindByUserName(string userName)
    {
        return await Entities.SingleOrDefaultAsync(x => x.UserName == userName);
    }

    public async Task<IList<User>> SearchUsers(string searchValue)
    {
        return await Entities.Where(x => x.UserName.StartsWith(searchValue)).ToListAsync();
    }
}