﻿using ChatApp.Domain.DataAccess;
using ChatApp.Domain.Entities;
using OA.Persistence;

namespace ChatApp.Persistence.Repositories;

public class MessageRepository: DataRepository<Message>, IMessageRepository
{
    public MessageRepository(ApplicationDbContext dataContext) : base(dataContext)
    {
    }
}