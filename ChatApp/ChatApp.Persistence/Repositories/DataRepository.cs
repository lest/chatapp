﻿using System.Linq.Expressions;
using ChatApp.Domain;
using ChatApp.Domain.DataAccess;
using Microsoft.EntityFrameworkCore;
using OA.Persistence;

namespace ChatApp.Persistence.Repositories;

public abstract class DataRepository<T> : IDataRepository<T> where T : BaseEntity
{
        private readonly ApplicationDbContext _dataContext;
        
        public virtual IQueryable<T> Entities => _dataContext.Set<T>().AsQueryable();
    
        protected DataRepository(ApplicationDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            _dataContext.Add(entity);
            await SaveChangesAsync();

            return entity;
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            _dataContext.AddRange(entities);
        }

        public virtual void Attach(T entity)
        {
            _dataContext.Attach(entity);
        }

        public virtual async Task SaveChangesAsync()
        {
            await _dataContext.SaveChangesAsync();
        }

        public virtual async Task<T?> Get(long id)
        {
            return await Entities.SingleOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task<List<T>> Get(IEnumerable<int> ids)
        {
            return await Entities.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public virtual async Task<T?> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return await Entities.FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<List<T>> ListAll()
        {
            return await Entities.ToListAsync();
        }

        public virtual async Task<bool> Any(Expression<Func<T, bool>> predicate)
        {
            return await Entities.AnyAsync(predicate);
        }

        public virtual async Task Remove(long id)
        {
            Remove(await Get(id));
        }

        public virtual async Task RemoveSoft(long id)
        {
            var entity = await Get(id);
            entity.IsDeleted = true;
        }

        public virtual void Remove(T entity)
        {
            _dataContext.Remove(entity);
        }

        public virtual async Task<List<T>> Where(Expression<Func<T, bool>> expression)
        {
            return await Entities.Where(expression).ToListAsync();
        }

        public async Task<int> Count() => await Entities.CountAsync();

    }