﻿using ChatApp.Domain.DataAccess;
using ChatApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using OA.Persistence;

namespace ChatApp.Persistence.Repositories;

public class DialogRepository: DataRepository<Dialog>, IDialogRepository
{
    public DialogRepository(ApplicationDbContext dataContext) : base(dataContext)
    {
    }
    
    public async Task<Dialog?> GetWithReferences(int dialogId)
    {
        return await Entities
            .Include(x => x.Messages)
            .Include(x => x.Participants)
            .SingleOrDefaultAsync(x => x.Id == dialogId);
    }
}