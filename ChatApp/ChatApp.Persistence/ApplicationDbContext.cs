﻿using ChatApp.Domain;
using Microsoft.EntityFrameworkCore;
using ChatApp.Domain.Entities;
using ChatApp.Persistence.Seeds;

namespace OA.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Dialog> Dialogs { get; set; }
        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Dialog>().ToTable("Dialog");
            modelBuilder.Entity<Message>().ToTable("Message");
            
            /*
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp without time zone")
                    .HasDefaultValueSql("NOW()")
                    .ValueGeneratedOnAdd();
            });
            
            modelBuilder.Entity<Dialog>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp without time zone")
                    .HasDefaultValueSql("NOW()")
                    .ValueGeneratedOnAdd();
            });
            
            modelBuilder.Entity<Message>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp without time zone")
                    .HasDefaultValueSql("NOW()")
                    .ValueGeneratedOnAdd();
            });
            */
            
            /*modelBuilder.Entity<Message>(entity =>
            {
                entity.Property(e => e.DateTimeUtc)
                    .IsRequired()
                    .HasColumnType("timestamp without time zone");
            });*/

            
            modelBuilder.Seed();
        }
    }
}
