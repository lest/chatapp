﻿using Microsoft.EntityFrameworkCore;
using ChatApp.Domain.Entities;

namespace OA.Persistence;

public interface IApplicationDbContext
{
    DbSet<User> Users { get; set; }
    DbSet<Dialog> Dialogs { get; set; }
    DbSet<Message> Messages { get; set; }
}