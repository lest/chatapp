﻿using ChatApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ChatApp.Persistence.Seeds
{
    public static class ContextSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            CreateBasicUsers(modelBuilder);
        }
        
        private static void CreateBasicUsers(ModelBuilder modelBuilder)
        {
            List<User> users = DefaultUser.CreateBasicUserList();
            modelBuilder.Entity<User>().HasData(users);
        }

    }
}
