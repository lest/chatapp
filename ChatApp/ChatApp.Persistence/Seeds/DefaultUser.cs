﻿using ChatApp.Domain.Entities;

namespace ChatApp.Persistence.Seeds;

public static class DefaultUser
{
    public static List<User> CreateBasicUserList()
    {
        return new List<User>
        {
            new()
            {
                Id = 1,
                UserName = "superadmin",
                // Password@123
                PasswordHash = "AQAAAAEAACcQAAAAEBLjouNqaeiVWbN0TbXUS3+ChW3d7aQIk/BQEkWBxlrdRRngp14b0BIH0Rp65qD6mA=="
            },
            new()
            {
                Id = 2,
                UserName = "testuser",
                // Password@123
                PasswordHash = "AQAAAAEAACcQAAAAEBLjouNqaeiVWbN0TbXUS3+ChW3d7aQIk/BQEkWBxlrdRRngp14b0BIH0Rp65qD6mA=="
            },
        };
    }
}