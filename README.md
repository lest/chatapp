# Chat application

# General.
Simple Chat application

# System requirements
.NET 6, Docker, node.js (LTS), Angular

# Development Environment Setup

## Setup
1. Go to root folder and run **db-up.cmd**
2. Go to **ChatApp.Client**. Open Console (cmd.exe) and run ```npm i```
3. To build(in serve mode) angular app, run **npm run start**
4. Don't forget apply migrations. Use ```dotnet ef database update```. Migrations project: **ChatApp.Persistence**. Startup project: **ChatApp**

oleg changes 